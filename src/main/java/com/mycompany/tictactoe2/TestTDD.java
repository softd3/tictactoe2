/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.tictactoe2;

import java.util.Scanner;

/**
 *
 * @author Aritsu
 */
class TestTDD {

    static String chup(char player1, char player2) {
        if(player1 == 'p' && player2 =='r'){
            return "p1";
        }else if(player1 == 's' && player2 == 'p'){
            return "p1";
        }else if(player1 == 'r' && player2 == 's'){
            return "p1";
        }else if(player1 == 'p' && player2 == 's'){
            return "p2";
        }else if(player1 == 's' && player2 == 'r'){
            return "p2";
        }else if(player1 == 'r' && player2 == 'p'){
            return "p2";
        }
        return "draw";
    }

    static int add(int a, int b) {
        return a+b;
    }
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.println("Please input player1 (p ,s ,r): ");
        String player1 = s.next();
        System.out.println("Please input player2 (p ,s ,r): ");
        String player2 = s.next();
        String winner = chup(player1.charAt(0),player2.charAt(0));
        System.out.println("Winner is "+winner);
        
    }
}
