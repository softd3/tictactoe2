/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.tictactoe2;

import java.util.Scanner;
import javax.swing.plaf.TableUI;

/**
 *
 * @author Aritsu
 */
public class OXv2 {

    static char table[][] = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    static char currentPlayer = 'O';
    static int row, col;
    static boolean finish = false;
    static int count = 0;

    public static void main(String[] args) {
        System.out.println("Welcome To OX Game");
        while (true) {
            showTable();
            showTurn();
            inputRowCol();
            process();
            if (finish) {
                break;
            }
        }

    }

    public static void showTable() {
        for (int i = 0; i < table.length; i++) {
            for (int j = 0; j < table.length; j++) {
                System.out.print(table[i][j]);
            }
            System.out.println();
        }
    }

    public static void showTurn() {
        System.out.println("Turn " + currentPlayer);
    }

    public static void inputRowCol() {
        Scanner s = new Scanner(System.in);
        System.out.println("Please input rol , col:");
        while (true) {
            row = s.nextInt();
            col = s.nextInt();
            if(row <= 3 && col <= 3){
                break;
            }else{
                System.out.println("Please input row and col 1-3 ");
            }
        }
    }

    public static void process() {
        if (setTable()) {
            if (checkWin(table , currentPlayer , row , col)) {
                finish = true;
                showWin();
                return;
            }
            if (checkDraw(count)) {
                finish = true;
                showDraw();
                return;
            }
            count++;
            swapPlayer();
        }
    }

    private static void showDraw() {
        showTable();
        System.out.println(">>>Draw<<<");
    }

    private static void showWin() {
        showTable();
        System.out.println(">>>" + currentPlayer + " Win<<<");
    }

    public static void swapPlayer() {
        if (currentPlayer == 'O') {
            currentPlayer = 'X';
        } else {
            currentPlayer = 'O';
        }
    }

    public static boolean setTable() {
        if(table[row-1][col-1] != '-'){
            System.out.println("That position has been already set Please try again");
            return false;
        }
        table[row - 1][col - 1] = currentPlayer;
        return true;
    }

    public static boolean checkWin(char[][] table , char currentPlayer , int row ,int col) {
        if (checkVertical(table , currentPlayer , col)) {
            return true;
        } else if (checkHorizontal( table , currentPlayer , row)) {
            return true;
        } else if (checkX( table , currentPlayer)) {
            return true;
        }

        return false;
    }

    public static boolean checkVertical(char[][] table , char currentPlayer , int col) {
        for (int r = 0; r < table.length; r++) {
            if (table[r][col - 1] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    public static boolean checkHorizontal(char[][] table , char currentPlayer , int row) {
        for (int c = 0; c < table.length; c++) {
            if (table[row - 1][c] != currentPlayer) {
                return false;
            }
        }
        return true;

    }

    public static boolean checkX(char[][] table , char currentPlayer) {
        if (checkX1(table, currentPlayer)) {
            return true;
        } else if (checkX2(table , currentPlayer)) {
            return true;
        }
        return false;
    }

    public static boolean checkX1(char[][] table , char currentPlayer) { // 11 , 22 , 33
        for (int i = 0; i < table.length; i++) {
            if (table[i][i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    public static boolean checkX2(char[][] table , char currentPlayer) { //13 , 22 , 31
        for (int i = 0; i < table.length; i++) {
            if (table[i][2 - i] != currentPlayer) {
                return false;
            }
        }

        return true;
    }

    public static boolean checkDraw(int count) {
        if (count >= 8) {
            return true;
        }
        return false;
    }

}
