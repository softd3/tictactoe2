/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.tictactoe2;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Aritsu
 */
public class TDDTest {
    
    public TDDTest() {
     
        
    }
    
    public void testAdd_3_4is7(){
        assertEquals(7,TestTDD.add(3,4));
    }
    
    
    
    @Test
    public void testChup_p1_p_p2_p_is_draw(){
        assertEquals("draw",TestTDD.chup('p','p'));
        
    }
    
    @Test
    public void testChup_p1_r_p2_r_is_draw(){ 
        assertEquals("draw",TestTDD.chup('r','r'));
        
    }
    
    @Test
    public void testChup_p1_s_p2_s_is_draw(){ 
        assertEquals("draw",TestTDD.chup('s','s'));
        
    }
    
    @Test
    public void testChup_p1_p_p2_r_is_p1(){ 
        assertEquals("p1",TestTDD.chup('p','r'));
        
    }
    
    @Test
    public void testChup_p1_s_p2_p_is_p1(){ 
        assertEquals("p1",TestTDD.chup('s','p'));
        
    }
    
    @Test
    public void testChup_p1_r_p2_s_is_p1(){ 
        assertEquals("p1",TestTDD.chup('r','s'));
        
    }
    
    @Test
    public void testChup_p1_p_p2_s_is_p2(){ 
        assertEquals("p2",TestTDD.chup('p','s'));
        
    }
    
    @Test
    public void testChup_p1_s_p2_r_is_p2(){ 
        assertEquals("p2",TestTDD.chup('s','r'));
        
    }
    
    @Test
    public void testChup_p1_r_p2_p_is_p2(){ 
        assertEquals("p2",TestTDD.chup('r','p'));
        
    }
    
    
    
}
