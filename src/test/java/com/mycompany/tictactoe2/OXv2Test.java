package com.mycompany.tictactoe2;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.mycompany.tictactoe2.OXv2;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Aritsu
 */
public class OXv2Test {
    
    public OXv2Test() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    
     @Test
     // checkVertical Player O
     public void testCheckVerticalPlayerOCol1Win() {
         char table[][] = {{'O', '-', '-'}, 
                                  {'O', '-', '-'}, 
                                  {'O', '-', '-'}};
         char currentPlayer = 'O';
         int col = 1;
         assertEquals(true,OXv2.checkVertical(table , currentPlayer , col));
     }
     @Test
     public void testCheckVerticalPlayerOCol2Win() {
         char table[][] = {{'-', 'O', '-'}, 
                                  {'-', 'O', '-'}, 
                                  {'-', 'O', '-'}};
         char currentPlayer = 'O';
         int col = 2;
         assertEquals(true,OXv2.checkVertical(table , currentPlayer , col));
     }
     @Test
     public void testCheckVerticalPlayerOCol3Win() {
         char table[][] = {{'-', '-', 'O'}, 
                                  {'-', '-', 'O'}, 
                                  {'-', '-', 'O'}};
         char currentPlayer = 'O';
         int col = 3;
         assertEquals(true,OXv2.checkVertical(table , currentPlayer , col));
     }
     
     
     // checkVertical Player X
     @Test
     public void testCheckVerticalPlayerXCol1Win() {
         char table[][] = {{'X', '-', '-'}, 
                                  {'X', '-', '-'}, 
                                  {'X', '-', '-'}};
         char currentPlayer = 'X';
         int col = 1;
         assertEquals(true,OXv2.checkVertical(table , currentPlayer , col));
     }
     @Test
     public void testCheckVerticalPlayerXCol2Win() {
         char table[][] = {{'-', 'X', '-'}, 
                                  {'-', 'X', '-'}, 
                                  {'-', 'X', '-'}};
         char currentPlayer = 'X';
         int col = 2;
         assertEquals(true,OXv2.checkVertical(table , currentPlayer , col));
     }
     @Test
     public void testCheckVerticalPlayerXCol3Win() {
         char table[][] = {{'-', '-', 'X'}, 
                                  {'-', '-', 'X'}, 
                                  {'-', '-', 'X'}};
         char currentPlayer = 'X';
         int col = 3;
         assertEquals(true,OXv2.checkVertical(table , currentPlayer , col));
     }
     
     //checkHorizontal Player O
     @Test
     public void testCheckHorizontalPlayerORow1Win() {
         char table[][] = {{'O', 'O', 'O'}, 
                                  {'-', '-', '-'}, 
                                  {'-', '-', '-'}};
         char currentPlayer = 'O';
         int row = 1;
         assertEquals(true,OXv2.checkHorizontal(table , currentPlayer , row));
     }
     @Test
     public void testCheckHorizontalPlayerORow2Win() {
         char table[][] = {{'-', '-', '-'}, 
                                  {'O', 'O', 'O'}, 
                                  {'-', '-', '-'}};
         char currentPlayer = 'O';
         int row = 2;
         assertEquals(true,OXv2.checkHorizontal(table , currentPlayer , row));
     }
     @Test
     public void testCheckHorizontalPlayerORow3Win() {
         char table[][] = {{'-', '-', '-'}, 
                                  {'-', '-', '-'}, 
                                  {'O', 'O', 'O'}};
         char currentPlayer = 'O';
         int row = 3;
         assertEquals(true,OXv2.checkHorizontal(table , currentPlayer , row));
     }
        
    //checkHorizontal Player X
     @Test
     public void testCheckHorizontalPlayerXRow1Win() {
         char table[][] = {{'X', 'X', 'X'}, 
                                  {'-', '-', '-'}, 
                                  {'-', '-', '-'}};
         char currentPlayer = 'X';
         int row = 1;
         assertEquals(true,OXv2.checkHorizontal(table , currentPlayer , row));
     }
     @Test
     public void testCheckHorizontalPlayerXRow2Win() {
         char table[][] = {{'-', '-', '-'}, 
                                  {'X', 'X', 'X'}, 
                                  {'-', '-', '-'}};
         char currentPlayer = 'X';
         int row = 2;
         assertEquals(true,OXv2.checkHorizontal(table , currentPlayer , row));
     }
     @Test
     public void testCheckHorizontalPlayerXRow3Win() {
         char table[][] = {{'-', '-', '-'}, 
                                  {'-', '-', '-'}, 
                                  {'X', 'X', 'X'}};
         char currentPlayer = 'X';
         int row = 3;
         assertEquals(true,OXv2.checkHorizontal(table , currentPlayer, row));
     }
     

     // checkX 1 player O
     @Test
     public void testCheckX1PlayerOWin() {
         char table[][] = {{'O', '-', '-'}, 
                                  {'-', 'O', '-'}, 
                                  {'-', '-', 'O'}};
         char currentPlayer = 'O';
         assertEquals(true,OXv2.checkX1(table , currentPlayer));
     }
     @Test
     public void testCheckX2PlayerOWin() {
         char table[][] = {{'-', '-', 'O'}, 
                                  {'-', 'O', '-'}, 
                                  {'O', '-', '-'}};
         char currentPlayer = 'O';
         assertEquals(true,OXv2.checkX2(table , currentPlayer));
     }
     
     @Test
     public void testCheckX1PlayerXWin() {
         char table[][] = {{'X', '-', '-'}, 
                                  {'-', 'X', '-'}, 
                                  {'-', '-', 'X'}};
         char currentPlayer = 'X';
         assertEquals(true,OXv2.checkX1(table , currentPlayer));
     }
     @Test
     public void testCheckX2PlayerXWin() {
         char table[][] = {{'-', '-', 'X'}, 
                                  {'-', 'X', '-'}, 
                                  {'X', '-', '-'}};
         char currentPlayer = 'X';
         assertEquals(true,OXv2.checkX2(table , currentPlayer));
     }
     
     // checkWin player O
     
     @Test
     public void testCheckWinPlayerO() {
         char table[][] = {{'O', '-', 'O'}, 
                                  {'X', 'O', 'X'}, 
                                  {'X', '-', 'O'}};
         char currentPlayer = 'O';
         int row = 1;
         int col = 2;
                
         assertEquals(true,OXv2.checkWin(table , currentPlayer,row , col ));
     }
    
     // checkWin player O

     @Test
     public void testCheckWinPlayerX() {
         char table[][] = {{'X', 'O', 'O'}, 
                                  {'X', 'O', 'X'}, 
                                  {'X', '-', 'O'}};
         char currentPlayer = 'X';
         int row = 1;
         int col = 1;
                
         assertEquals(true,OXv2.checkWin(table , currentPlayer,row , col ));
     }

     // checkDraw
     @Test
     public void testCheckDraw() {
         char table[][] = {{'X', 'O', 'O'}, 
                                  {'O', 'X', 'X'}, 
                                  {'O', 'X', 'O'}};
         int count = 8;
         assertEquals(true,OXv2.checkDraw(count));
     }
     @Test
     public void testCheckDraw2() {
         char table[][] = {{'O', 'X', 'O'}, 
                                  {'-', 'O', 'X'}, 
                                  {'-', 'X', 'O'}};
         int count = 7;
         assertEquals(false,OXv2.checkDraw(count));
     }
}
